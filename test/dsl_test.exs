ExUnit.start()

Code.require_file("../dsl.ex", __DIR__)

defmodule DoReMi do
  use DSL

  sequence :do_re_mi do
    note(:c, octet: 4, volume: 50, duration: 0.25)
    note(:d, modifier: :base, duration: 0.25)
    note(:e, modifier: :base, duration: 0.25)
  end

  sequence :fa_so_la_ti_do do
    note(:f, octet: 4, volume: 50, duration: 0.25)
    note(:g, modifier: :base, duration: 0.25)
    note(:a, duration: 0.25)
    note(:b, duration: 0.25)
    note(:c, octet: 5, duration: 0.25)
  end

  sequence :song do
    embed_notes(:do_re_mi)
    embed_notes(:fa_so_la_ti_do)
  end
end

defmodule DSLTest do
  use ExUnit.Case
  import ExUnit.CaptureIO

  describe "DoReMi.__sequence_do_re_mi__/0" do
    test "returns expected notes" do
      expected_notes = [
        %Note{class: :c, duration: 0.25},
        %Note{class: :d, duration: 0.25},
        %Note{class: :e, duration: 0.25}
      ]

      assert DoReMi.__sequence_do_re_mi__() == expected_notes
    end
  end

  describe "DoReMi.__sequence_song__/0" do
    test "returns expected notes" do
      expected_notes = [
        %Note{class: :c, duration: 0.25},
        %Note{class: :d, duration: 0.25},
        %Note{class: :e, duration: 0.25},
        %Note{class: :f, duration: 0.25},
        %Note{class: :g, duration: 0.25},
        %Note{class: :a, duration: 0.25},
        %Note{class: :b, duration: 0.25},
        %Note{class: :c, duration: 0.25, octet: 5}
      ]

      assert DoReMi.__sequence_song__() == expected_notes
    end
  end

  # Uncomment this when ready!
  describe "DoReMi.play/1" do
    test "plays the song (not testing alsa)" do
      assert capture_io(fn ->
               DoReMi.play(:do_re_mi)
             end) =~ ""
    end
  end
end
