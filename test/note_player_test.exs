ExUnit.start()

defmodule NotePlayerTest do
  use ExUnit.Case
  import ExUnit.CaptureIO

  Code.require_file("../note_player.ex", __DIR__)

  @module NotePlayer

  describe "play/1" do
    test "plays a note (note testing alsa)" do
      note = struct!(Note, %{class: :a, octet: 4})

      assert capture_io(fn ->
               assert @module.play(note) == :ok
             end) =~ ""
    end
  end
end
