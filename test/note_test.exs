ExUnit.start()

defmodule NoteTest do
  use ExUnit.Case

  Code.require_file("../note.ex", __DIR__)

  @module Note

  describe "defstruct with proper keys" do
    test "has expected fields" do
      expected_keys = ~w(class modifier octet duration volume)a

      final_keys =
        @module.__struct__()
        |> Map.keys()
        |> List.delete(:__struct__)

      assert Enum.sort(expected_keys) == Enum.sort(final_keys)
    end
  end

  describe "to_frequency/1" do
    test "converts a non-rest note to frequency" do
      note = struct!(@module, %{class: :a, octet: 4})

      assert @module.to_frequency(note) == 440
    end

    test "converts a rest note to frequency" do
      note = struct!(@module, %{class: :rest, octet: 4})

      assert @module.to_frequency(note) == 0
    end
  end
end
