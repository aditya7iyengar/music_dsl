defmodule DSL do
  Code.require_file("note.ex", __DIR__)
  Code.require_file("note_player.ex", __DIR__)

  defmacro __using__(_) do
    quote do
      import unquote(__MODULE__)

      def play(sequence_name) do
        __MODULE__
        |> apply(:"__sequence_#{sequence_name}__", [])
        |> Enum.map(&NotePlayer.play/1)
      end
    end
  end

  defmacro sequence(sequence_name, do: block_ast) do
    quote location: :keep do
      def unquote(:"__sequence_#{sequence_name}__")() do
        {:ok, var!(pid)} = start_sequence()
        unquote(block_ast)
        notes = get_sequence_notes(var!(pid))
        stop_sequence(var!(pid))
        notes
      end
    end
  end

  defmacro note(class, opts) do
    quote location: :keep do
      note = note_from_options(unquote(class), unquote(opts))
      add_note(var!(pid), note)
    end
  end

  defmacro embed_notes(sequence_name) do
    quote location: :keep do
      __MODULE__
      |> apply(:"__sequence_#{unquote(sequence_name)}__", [])
      |> Enum.map(&add_note(var!(pid), &1))
    end
  end

  def start_sequence, do: Agent.start_link(fn -> [] end)
  def get_sequence_notes(pid), do: Agent.get(pid, &Enum.reverse/1)
  def stop_sequence(pid), do: Agent.stop(pid)
  def add_note(pid, note), do: Agent.update(pid, &[note | &1])

  def note_from_options(class, opts) do
    params =
      opts
      |> Keyword.put(:class, class)
      |> Enum.into(%{})

    struct!(Note, params)
  end
end
