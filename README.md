---
title: Metaprogramming 101 & DSL Design in Elixir
author: Adi Iyengar
patat:
    incrementalLists: true
    theme:
        emph: [vividWhite, onVividBlack, bold]
        imageTarget: [onDullWhite, vividRed]
---


# Agenda

- Build a simple DSL to compose and play music by writing Elixir code

- DSL should look something like this:

```elixir
defmodule Music do
  sequence :intro do
    # A note has a class, modifier, octet, duration and volume
    note :c, modifier: :sharp, octet: 4, duration: 0.5, volume: 50
    # Some are defaults
    note :d, modifier: :base, octet: 4, duration: 0.5
    # This is equivalent to no note...
    note :rest, octet: 0, duration: 0.5
    note :e, octet: 4, duration: 0.5
  end

  sequence :break do
    # Default duration is 0.5
    note :rest, 0
  end

  sequence :outro do
    note :c, octet: 4, modifier: :sharp, duration: 0.5
    note :d, octet: 4, modifier: :sharp, duration: 0.5
  end

  sequence :final do
    embed_notes :intro
    embed_notes :break
    embed_notes :outro
  end
end

# MusicTest.play(:final)
```


# Tasks

- Play a note using the `aplay` command:
```
echo 'foo' |  awk '{ for (i = 0; i < 0.75; i+= 0.00003125) printf("%08X\n", 50*sin(440*3.14*exp((a[$1 % 8]/12)*log(2))*i)) }' | xxd -r -p | aplay -c 2 -f S32_LE -r 28000
```

- Define `Note` struct

- Covert a note to a frequency using a4's base frequency: 440

```elixir
semitones_from_a4 = 12 * (note.octet - 4) - (9 - semitone_index(note))
relative_frequency = pow(1.059463, semitones_from_a4)
a4_frequency * relative_frequency # round

## To get semitone index:
[
  {:c, :base},
  {:c, :sharp},
  {:d, :base},
  {:d, :sharp},
  {:e, :base},
  {:f, :base},
  {:f, :sharp},
  {:g, :base},
  {:g, :sharp},
  {:a, :base},
  {:a, :sharp},
  {:b, :base}
]
```

- Define a NotePlayer module that plays give note:

```elixir
echo 'foo' |  awk '{ for (i = 0; i < #{duration}; i+= 0.00003125) \
printf("%08X\\n", #{volume}*sin(#{frequency}*3.14*exp((a[$1 % 8]/12)\
*log(2))*i)) }' | xxd -r -p | aplay -c 2 -f S32_LE -r 28000
```

- Write the DSL

- Run the Final track: `elixir final.exs`
